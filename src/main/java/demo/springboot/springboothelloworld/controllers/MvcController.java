package demo.springboot.springboothelloworld.controllers;

import demo.springboot.springboothelloworld.models.Person;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Fahad Ahmed
 */
@Controller
@RequestMapping("")
public class MvcController {

    @GetMapping(path = {"/", "/home"})
    public @ResponseBody String home(){
        return "Hello Spring Boot and Spring MVC!";
    }

    @GetMapping(value = "/person")
    public @ResponseBody Person getPerson(){
        return new Person("John", "Smith");
    }
}
