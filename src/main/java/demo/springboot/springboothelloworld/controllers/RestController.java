package demo.springboot.springboothelloworld.controllers;

import demo.springboot.springboothelloworld.models.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Fahad Ahmed
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {

    @GetMapping
    public String home(){
        return "Hello World from Spring Boot and Spring Rest";
    }

    @GetMapping(value = "/person")
    public @ResponseBody Person getPerson(){
        return new Person("John", "Smith");
    }

}
